<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Controller;

#use longlang\phpkafka\Producer\Producer;
#use longlang\phpkafka\Producer\ProducerConfig;
use SleekDB\Query;

class IndexController extends AbstractController
{
    public function index()
    {
        $user = $this->request->input('user', 'Hyperf');
        $method = $this->request->getMethod();
/*
	$config = new ProducerConfig();
$config->setBootstrapServer('127.0.0.1:9092');
$config->setUpdateBrokers(true);
$config->setAcks(-1);
$producer = new Producer($config);
$topic = 'test';
$value = (string) microtime(true);
$key = uniqid('', true);
$producer->send('test', $value, $key);
 */

	$databaseDirectory = __DIR__ . "/myDatabase";
	$configuration = [
  "auto_cache" => true,
  "cache_lifetime" => null,
  "timeout" => false,
  "primary_key" => "_id",
  "search" => [
    "min_length" => 2,
    "mode" => "or",
    "score_key" => "scoreKey",
    "algorithm" => Query::SEARCH_ALGORITHM["hits"]
  ],
  "folder_permissions" => 0777
];
        $newsStore = new \SleekDB\Store("news", $databaseDirectory, $configuration);
	$article = [
 "title" => "Google Pixel XL",
 "about" => "Google announced a new Pixel!",
 "author" => [
   "avatar" => "profile-12.jpg",
   "name" => "Foo Bar"
 ]
];
	$results = $newsStore->insert($article);
	$allNews = $newsStore->findAll();

print_r($allNews);
        return [
            'method' => $method,
            'message' => "Hello {$user}.",
        ];
    }
}
